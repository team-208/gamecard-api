## GameCard API

REST API to create/read/update/delete game cards

## Start with docker

```shell script
$ docker network create poly-paint-network
```
```shell script
$ docker-compose up
```
## Start with your IDE

1- Only start the Mongo database
```shell script
$ docker-compose up db
```

2- Install Python 3.7.+
```shell script
$ sudo apt update
$ sudo apt install software-properties-common
$ sudo add-apt-repository ppa:deadsnakes/ppa
$ sudo apt install python3.7
```

3- Install Pip (Python Package Manger)
```shell script
$ sudo apt-get install python3-pip
```

4- Install dependencies
```shell script
$ virtualenv --python=python3.7 django-env
$ source django-env/bin/activate
$ pip install -r requirements.txt
```

5- Start server on port 8000
```shell script
$ python manage.py runserver
```
