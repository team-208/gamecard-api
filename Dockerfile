FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/app

COPY requirements.txt .

RUN python3 -m pip install -r requirements.txt --no-cache-dir

COPY . .

EXPOSE 5000
CMD ["python", "manage.py", "runserver", "0.0.0.0:5000"]
