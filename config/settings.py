import os
import sys

import mongoengine

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '6%9nwl66i1!e4h3q6uaycj!&d%n@lox0ivebg@0j1ly^c8as6t'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',

    'rest_framework',
    'rest_framework_mongoengine',

    'gamecard'
]

MIDDLEWARE = [
    'django.middleware.common.CommonMiddleware',
]

ROOT_URLCONF = 'config.urls'

WSGI_APPLICATION = 'config.wsgi.application'

# Dont connect to db if in test mode
if 'test' in sys.argv or 'test_coverage' in sys.argv:
    mongoengine.connect(
        db="testdb",
        host='mongomock://localhost',
    )
else:
    mongoengine.connect(
        db="gamecard",
        host=os.environ.get('DATABASE_HOST', '127.0.0.1'),
        port=int(os.environ.get('DATABASE_PORT', '27017')),
        username=os.environ.get('DATABASE_USER', 'docker'),
        password=os.environ.get('DATABASE_PASSWORD', 'docker'),
    )
