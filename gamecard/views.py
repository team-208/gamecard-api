from random import randint

from rest_framework import exceptions
from rest_framework.parsers import FileUploadParser
from rest_framework_mongoengine.generics import ListCreateAPIView, RetrieveDestroyAPIView, ListAPIView

from gamecard.models import GameCard
from gamecard.serializers import GameCardSerializer


class ListCreateGameCard(ListCreateAPIView):
    serializer_class = GameCardSerializer
    queryset = GameCard.objects.all()
    parser_class = (FileUploadParser,)


class RetrieveDestroyGameCard(RetrieveDestroyAPIView):
    serializer_class = GameCardSerializer
    queryset = GameCard.objects.all()


class RandomGameCard(ListAPIView):
    serializer_class = GameCardSerializer
    queryset = GameCard.objects.all()
    query_param = 'amount'

    def filter_queryset(self, queryset):
        amount: str = self.request.query_params.get(self.query_param)
        if not amount or not amount.isdigit():
            raise exceptions.ValidationError({
                self.query_param: 'Query param is invalid'
            })

        count = queryset.count()
        if int(amount) > count:
            raise exceptions.ValidationError({
                self.query_param: 'Requested more game cards then available'
            })

        game_cards_set = set()
        while len(game_cards_set) < int(amount):
            random_index = randint(0, count - 1)
            game_card = queryset[random_index]
            game_cards_set.add(game_card)

        return game_cards_set
