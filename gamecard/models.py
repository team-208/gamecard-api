from enum import Enum

from mongoengine import Document, fields, EmbeddedDocument, StringField, ListField


class DrawMode(Enum):
    NORMAL = 'NORMAL'
    RANDOM = 'RANDOM'
    PANORAMIC_LEFT = 'PANORAMIC_LEFT'
    PANORAMIC_RIGHT = 'PANORAMIC_RIGHT'
    PANORAMIC_TOP = 'PANORAMIC_TOP'
    PANORAMIC_BOTTOM = 'PANORAMIC_BOTTOM'
    CENTERED_INWARD = 'CENTERED_INWARD'
    CENTERED_OUTWARD = 'CENTERED_OUTWARD'


class Difficulty(Enum):
    EASY = 'EASY'
    NORMAL = 'NORMAL'
    HARD = 'HARD'


class HeadType(Enum):
    ROUND_STYLUS = "ROUND_STYLUS"
    SQUARE_STYLUS = "SQUARE_STYLUS"
    POINT_ERASER = "POINT_ERASER"
    STROKE_ERASER = "STROKE_ERASER"


class BoundedListField(ListField):

    def __init__(self, min_size: int, max_size: int, **kwargs):
        self.min_size = min_size if min_size else 0
        self.max_size = max_size
        super(BoundedListField, self).__init__(**kwargs)

    def validate(self, value):
        super(BoundedListField, self).validate(value)

        if self.max_size is not None and len(value) > self.max_size:
            self.error("List should have at most <{}> objects".format(self.max_size))

        if len(value) < self.min_size:
            self.error("List should have at least <{}> objects".format(self.min_size))


class Point(EmbeddedDocument):
    x = fields.IntField(required=True)
    y = fields.IntField(required=True)


class Stroke(EmbeddedDocument):
    points = fields.EmbeddedDocumentListField(document_type=Point, required=True)
    size = fields.IntField(required=True)
    z_index = fields.IntField(required=True)
    color = fields.StringField(required=True)
    head_type = fields.StringField(required=True,
                                   choices=[(e.value, e.name) for e in HeadType],
                                   default=HeadType.ROUND_STYLUS.value)


class Drawing(EmbeddedDocument):
    strokes = fields.EmbeddedDocumentListField(document_type=Stroke, required=True)
    word = fields.StringField(required=True, min_length=2, max_length=15)


class GameCard(Document):
    difficulty = fields.StringField(required=True,
                                    choices=[(e.value, e.name) for e in Difficulty],
                                    default=Difficulty.NORMAL.value)
    draw_mode = fields.StringField(required=True,
                                   choices=[(e.value, e.name) for e in DrawMode],
                                   default=DrawMode.NORMAL.value)
    drawing = fields.EmbeddedDocumentField(document_type=Drawing, required=True)
    hints = BoundedListField(1, 128, field=StringField(max_length=128), required=True)
