from rest_framework_mongoengine import serializers

from gamecard.models import GameCard


class GameCardSerializer(serializers.DocumentSerializer):

    class Meta:
        model = GameCard
        fields = '__all__'
        extra_kwargs = {
            'id': {
                'read_only': True
            },
        }
