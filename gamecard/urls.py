from django.urls import path

from gamecard.views import ListCreateGameCard, RetrieveDestroyGameCard, RandomGameCard

urlpatterns = [
    path('gamecard/random', RandomGameCard.as_view()),
    path('gamecard/<id>', RetrieveDestroyGameCard.as_view()),
    path('gamecard', ListCreateGameCard.as_view()),
]
