from django.apps import AppConfig


class GamecardConfig(AppConfig):
    name = 'gamecard'
